package cn.boodqian.airreport;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Locale;


import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.widget.TextView;

import cz.msebera.android.httpclient.Header;
import timber.log.Timber;

public class ServerMessageHandler extends TextHttpResponseHandler {
    public ServerMessageHandler(Activity mParentActivity) {
        super();
        this.mParentActivity = mParentActivity;
    }
    private Activity mParentActivity;
    
    // Whether this is checked by user manually
    // If so, need to ignore server message and to prompt if this is already the newest version
    public boolean mUserCommand = false;

    @Override
    public void onSuccess(int statusCode, Header[] headers, String response) {
    	Timber.d("response="+response);

        // For debug purpose
        /*
        response = "{" +
        		"\"version\":40,\r\n" +
        		"\"apk\":\"airreport-v2.1-25-release.apk\",\r\n" + 
        		"\"update-zh\":\"新版本提示\"," + 
        		"\"update\":\"Update Message\"," +
        		"\"message-zh\":\"服务器消息\"," +
        		"\"message\":\"Server Message\"," +
        		"\"message-code\":5" + 
        		"}";
         //*/
        
        HashMap<String,String> map = null;
        try {
            Type type = new TypeToken<HashMap<String,String>>(){}.getType();
            map = GlobalData.gGson.fromJson(response, type);
        } catch(JsonParseException e) {
            map = null;
            Timber.e(e, "Error parsing server message");
        }
        if(map==null || map.size()==0) {
            Timber.w("Received invalid update message");
            return;
        }

        Timber.i("Server Message: "+map.toString());

        String locale = Locale.getDefault().getLanguage();
        if( locale.length() >= 2 ) locale = locale.substring(0, 2);
        else locale = "";
        Timber.i("Client locale: " + locale);

        long when = System.currentTimeMillis();
        int currentVersion = GlobalData.getVersionCode(mParentActivity);
        int messageVersion = 0;
        int savedLastServerMessageCode = GlobalData.gLastServerMessageCode;
        long savedLastUpdateMessageTime = GlobalData.gLastUpdateMessageTime;
        try{
            messageVersion = Integer.parseInt(map.get("version"));
            Timber.i(String.format(java.util.Locale.US, "Server Message rcv, %d (%d)", messageVersion, currentVersion));

            if(currentVersion>messageVersion) return;

            String message = null;
            String title = null;
            int messageCode=0;
            boolean isLatestVersion = ( currentVersion == messageVersion );
            Timber.i( String.format("isLatestVersion: %b, mUserCommand: %b", isLatestVersion, mUserCommand ) );

            if( !isLatestVersion ) {
                // When version below, show 'update' field
                title = mParentActivity.getString(R.string.server_update);
                message = map.get("update-" + locale);
                if( message == null )
                    message = map.get("update");
            } else if(  isLatestVersion && !mUserCommand ) {
                // When version matches, show 'message' field
                title = mParentActivity.getString(R.string.server_message);
                message = map.get("message-" + locale);
                if( message == null )
                    message = map.get("message");
                String str = map.get("message-code");
                if(str!=null)
                    messageCode = Integer.parseInt(str);

                Timber.i(String.format(java.util.Locale.US, "message-code: old %d new %d", GlobalData.gLastServerMessageCode, messageCode));

                // Only display message with higher code
                if(GlobalData.gLastServerMessageCode >= messageCode) return;
            }
            if(message==null || message.compareTo("")==0) return;

            // Only prompt once a day at most
            // Update the time stamp now so we don't retrieve the update info next time
            GlobalData.gLastUpdateMessageTime = when;
            
            if( !isLatestVersion )
                showUpdateDialog(title, message, map.get("apk") );
            else
                showMessageDialog(title, message);
        } catch (Exception e) {
            if(currentVersion == messageVersion)
                GlobalData.gLastServerMessageCode = savedLastServerMessageCode;
            else
                GlobalData.gLastUpdateMessageTime = savedLastUpdateMessageTime;
            Timber.e(e, "Parse server message failed");
        } finally {
            if( mUserCommand && currentVersion >= messageVersion ) {
                new AlertDialog.Builder(mParentActivity).setTitle(android.R.string.dialog_alert_title)
                .setMessage(R.string.no_update_available)
                .setPositiveButton(R.string.iknow, null)
                .show();
            }

            // Always reset user command flag
            if( mUserCommand ) mUserCommand = false;
        }
    }
        
    @Override
    public void onFailure(int statusCode, Header[] headers,
    		String responseString, Throwable throwable) {
    	Timber.e(throwable, "Check update error");

        // Always reset user command flag
        if( mUserCommand ) mUserCommand = false;
    }

    private void showMessageDialog( String title, String message )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mParentActivity).setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.iknow, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            dialog.dismiss();
                        } catch( Exception e ) {
                          // In case activity finished before dismiss (several hits seen in crash report)
                          Timber.e(e, "I know Dismiss exception");
                        }
                    }
                });
        AlertDialog dialog = builder.show();
        TextView textView = (TextView) dialog.findViewById(android.R.id.message);
        textView.setTextSize(15);
    }
    private void showUpdateDialog( String title, String message, String apkUrl )
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(mParentActivity).setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.check_store, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(mParentActivity.getString(R.string.marketlink)));
                        try {
                            mParentActivity.startActivity(intent);
                        } catch(ActivityNotFoundException e) {
                            new AlertDialog.Builder(mParentActivity).setTitle(android.R.string.dialog_alert_title)
                            .setMessage(R.string.no_store_installed)
                            .setPositiveButton(R.string.iknow, null)
                            .show();
                        }
                    }
                });

        if( ! apkUrl.startsWith("http://") )
            apkUrl = GlobalData.gApkHost+apkUrl;
        Timber.i("update apk: " + apkUrl);
        final String apk = apkUrl;
        if( apk != null && apk.endsWith(".apk") ) {
            builder.setNegativeButton(R.string.direct_download, new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(apk));
                    try {
                        mParentActivity.startActivity(intent);
                    } catch(ActivityNotFoundException e) {
                        new AlertDialog.Builder(mParentActivity).setTitle(android.R.string.dialog_alert_title)
                        .setMessage(R.string.no_browser_installed)
                        .setPositiveButton(R.string.iknow, null)
                        .show();
                    }
                }
            });
        }
        
        builder.setNeutralButton(R.string.remind_later, new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        dialog.dismiss();
                    } catch( Exception e ) {
                        // In case activity finished before dismiss (several hits seen in crash report)
                        Timber.e(e, "Reminder Later Dismiss exception");
                    }
                }
        });
        
        AlertDialog dialog = builder.show();
        TextView textView = (TextView) dialog.findViewById(android.R.id.message);
        textView.setTextSize(15);
    }
}