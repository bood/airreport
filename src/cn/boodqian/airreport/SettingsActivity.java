package cn.boodqian.airreport;

import android.app.ActionBar;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.view.MenuItem;

public class SettingsActivity extends PreferenceActivity
                                         implements OnSharedPreferenceChangeListener {
    public static final String WIDGET_CLICK_HOME = "home";
    public static final String WIDGET_CLICK_REFRESH = "refresh";
    private ListPreference mAQIStandardPreference;
    private Preference mWidgetTransparencyPreference;
    private ListPreference mWidgetClickPreference;
    private ListPreference mNotificationThresholdPreference;
    
    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

      mAQIStandardPreference = (ListPreference) findPreference( getString(R.string.pref_name_aqi_standard ) );
        mWidgetClickPreference = (ListPreference) findPreference( getString(R.string.pref_name_widget_click ) );
        mWidgetTransparencyPreference = findPreference( getString(R.string.pref_name_widget_transparency ) );
        mNotificationThresholdPreference = (ListPreference) findPreference( getString(R.string.pref_name_notification_threshold ) );
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        	updateSummary();
        	
        	// Send update notification to all widgets
        	if( key.equals(getString(R.string.pref_name_widget_transparency )) ||
        	        key.equals(getString(R.string.pref_name_widget_click )) ||
        	        key.equals(getString(R.string.pref_name_aqi_standard )) ) {
        	    AppWidgetManager manager = AppWidgetManager.getInstance(this);
        	    final int[] appWidgetIds = manager.getAppWidgetIds(new ComponentName(this, AirreportWidgetProvider.class));
        	    
        	    Intent updateIntent = new Intent(this, AirreportWidgetProvider.class);
        	    updateIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        	    updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
        	    sendBroadcast(updateIntent);
        	}
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onResume() {
        super.onResume();
        updateSummary();
        getPreferenceScreen().getSharedPreferences()
        .registerOnSharedPreferenceChangeListener(this);
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
        .unregisterOnSharedPreferenceChangeListener(this);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            // app icon in action bar clicked; go home
            finish();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
    private void updateSummary() {
        CharSequence aqiStandard = mAQIStandardPreference.getEntry();
        mAQIStandardPreference.setSummary(aqiStandard);
        
        CharSequence action = mWidgetClickPreference.getEntry();
        mWidgetClickPreference.setSummary(action);        
        
        CharSequence threshold = mNotificationThresholdPreference.getEntry();
        mNotificationThresholdPreference.setSummary(
                String.format(getString(R.string.pref_notification_threshold_summary), threshold));
    }


}
