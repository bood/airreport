package cn.boodqian.airreport;

import android.app.Application;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.net.ssl.SSLException;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

public class AirreportApplication extends Application {
  @Override
  public void onCreate() {
    super.onCreate();

    Fabric.with(this, new Crashlytics());

    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
    }

    Timber.plant(new Timber.Tree() {
      @Override
      protected void log(int priority, String tag, String message, Throwable t) {
        if (priority >= Log.INFO && message != null) {
          Crashlytics.log(message);
        }
        if (priority >= Log.ERROR) {
          if (t != null
              && !(t instanceof UnknownHostException)
              && !(t instanceof SocketTimeoutException)
              && !(t instanceof SSLException)
              && !(t instanceof SocketException)) {
            Crashlytics.logException(t);
          }
        }
      }

      @Override
      protected boolean isLoggable(String tag, int priority) {
        return priority >= Log.INFO;
      }
    });
  }
}
