package cn.boodqian.airreport;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Bood on 8/15/2014.
 */
public class AirreportPagerAdapter extends PagerAdapter {
    public static final String PROFILE_TAG = "profile";
    public static final String HISTORY_TAG = "history";
    public static final String AQILEVEL_TAG = "aqilevel";
    public static final int PROFILE_POSITION = 0;
    public static final int HISTORY_POSITION = 1;
    public static final int AQILEVEL_POSITION = 2;

    private Activity mActivity;
    private SimpleExpandableListAdapter mProfileListAdapter;
    private String mHistoryName;
    private List<String> mHistoryMarks;
    private HashMap<String, List<Float>> mHistoryData;

    private ExpandableListView mProfileView;
    private HistoryView mHistoryView;
    private TextView mHistoryTitle;
    private static final int[] resIdList = new int[]{R.layout.profile, R.layout.history, R.layout.aqilevel};
    private static final String[] tagList = new String[]{PROFILE_TAG, HISTORY_TAG, AQILEVEL_TAG};

    public AirreportPagerAdapter(Activity activity) {
        mActivity = activity;
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater)container.getContext().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(resIdList[position], null);
        switch(resIdList[position]) {
            case R.layout.profile:
                mProfileView = (ExpandableListView) view.findViewById(R.id.list_profile);
                refreshProfileList();
                break;
            case R.layout.history:
                mHistoryView = (HistoryView) view.findViewById(R.id.history_view);
                mHistoryTitle = (TextView) view.findViewById(R.id.history_view_title);
                refreshHistoryView();
                break;
        }
        view.setTag(tagList[position]);
        ((ViewPager) container).addView(view);
        Timber.d("pager " + position + " created");
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
        Timber.d("pager " + position + " destroyed");
        switch(resIdList[position]) {
            case R.layout.profile:
                mActivity.unregisterForContextMenu(mProfileView);
                mProfileView = null;
                break;
            case R.layout.history:
                mHistoryView = null;
                mHistoryTitle = null;
                break;
        }
    }

    @Override
    public int getCount() {
        return resIdList.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==object);
    }

    public void setProfileListAdapter(SimpleExpandableListAdapter adapter) {
        mProfileListAdapter = adapter;
        refreshProfileList();
    }

    public void refreshProfileList() {
        if( mProfileView == null ) {
            Timber.w("Profile view is null");
            return;
        }
        mActivity.registerForContextMenu(mProfileView);
        mProfileView.setOnGroupClickListener((ExpandableListView.OnGroupClickListener) mActivity);
        if( mProfileListAdapter != null ) {
            mProfileView.setAdapter(mProfileListAdapter);
        }
    }

    public void setHistoryData(String name, List<String> marks, HashMap<String, List<Float>> data) {
        mHistoryName = name;
        mHistoryMarks = marks;
        mHistoryData = data;

        refreshHistoryView();
    }

    public void refreshHistoryView() {
        if (mHistoryView == null || mHistoryTitle == null) {
            Timber.w("History view is null");
            return;
        }
        if( mHistoryName == null || mHistoryMarks == null || mHistoryData == null ) {
            return;
        }

        mHistoryTitle.setText(mHistoryName);
        mHistoryView.clearHistoryData();
        mHistoryView.setHorizonMark(mHistoryMarks);
        for( String pollutant : mHistoryData.keySet() ) {
            List<Float> aqiList = mHistoryData.get(pollutant);
            mHistoryView.addHistoryData(
                    aqiList,
                    (int)GlobalData.getPollutantDisplayColor(pollutant),
                    GlobalData.getPollutantDisplayName(pollutant)
            );
        }
        mHistoryView.invalidate();
    }

    public ExpandableListView getProfileView() {
        return mProfileView;
    }
}
