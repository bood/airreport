package cn.boodqian.airreport;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class AirdataHttpClient {
	public static final String BASE_URL = "http://airdatafeed.sinaapp.com/";
	public static final String BACKUP_URL = "http://airdatafeed.vipsinaapp.com/";

	private static AsyncHttpClient client = new AsyncHttpClient();

	public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		client.get(getAbsoluteUrl(url), params, responseHandler);
	}

	public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		client.post(getAbsoluteUrl(url), params, responseHandler);
	}

	private static String getAbsoluteUrl(String relativeUrl) {
		return BASE_URL + relativeUrl;
	}
	
	public static void getAirdata(String locationJson, AsyncHttpResponseHandler responseHandler) {
		RequestParams params = new RequestParams();
		params.add("location", locationJson);
		post("airdata", params, responseHandler);
	}
	
	public static void getLocationList(String lang, AsyncHttpResponseHandler responseHandler) {
		RequestParams params = new RequestParams();
		params.add("lang", lang);
		get("location/list", params, responseHandler);
	}
	
	public static void getLocationCode(AsyncHttpResponseHandler responseHandler) {
		RequestParams params = new RequestParams();
		get("location/code", params, responseHandler);
	}
	
	public static void checkUpdate(AsyncHttpResponseHandler responseHandler) {
		RequestParams params = new RequestParams();
		get("update", params, responseHandler);
	}
}
