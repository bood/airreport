package cn.boodqian.airreport;

import android.support.annotation.ColorRes;

public class AQI {
	public static final int OUTRANGE = 500;
	public static final String US_STANDARD = "us";
	public static final String CN_STANDARD = "cn";
	/*
	 * cc (mg/m3) = ppm * M/22.4
	 * ppm = 22.4 * cc/M
	 */
	static float[] AQI_Breakpoints = { 0, 50, 100, 150, 200, 300, 400, 500 };
	// US Standard
	static double[][] Pollutant_Breakpoints_US = {
		{ 0, .0121, .0355, .0555, .1505, .2505, .3505, .5005 }, //PM2.5
		{ 0, .055, .155, .255, .355, .425, .505, .604 }, //PM10
		{ 0, .036*64/22.4, .076*64/22.4, .186*64/22.4, .304*64/22.4, .605*64/22.4, .805*64/22.4, 1.004*64/22.4 }, //SO2
		//{ 0, .036, .076, .186, .304, -1, -1, -1 }, //SO2_1h, M=32+32=64
		//{ -1, -1, -1, -1, .304, .605, .805, 1.004 }, //SO2_24h
		{ 0, .054*46/22.4, .101*46/22.4, .361*46/22.4, .650*46/22.4, 1.25*46/22.4, 1.65*46/22.4, 2.049*46/22.4 }, //NO2, M=14+16*2=46
		{ 0, 4.5*28/22.4, 9.5*28/22.4, 12.5*28/22.4, 15.5*28/22.4, 30.5*28/22.4, 40.5*28/22.4, 50.5*28/22.4 }, //CO, M=12+16=28
		{ 0, .0625*48/22.4, .125*48/22.4, .165*48/22.4, .205*48/22.4, .405*48/22.4, .505*48/22.4, .605*48/22.4 }, //O3, M=16*3=48
		//{}, //O3_8h
	};
	
	// China Standard
	static double[][] Pollutant_Breakpoints_CN = {
		{ 0, .035, .075, .115, .150, .250, .350, .500 }, //PM2.5
		{ 0, .050, .150, .250, .350, .420, .500, .600 }, //PM10
		{ 0, .150, .500, .650, .800, 1.600, 2.100, 2.620 }, //SO2
		{ 0, .100, .200, .700, 1.200, 2.340, 3.090, 3.840 }, //NO2, M=14+16*2=46
		{ 0, 5, 10, 35, 60, 90, 120, 150 }, //CO, M=12+16=28
		{ 0, .160, .200, .300, .400, .800, 1.000, 1.200 }, //O3_1h, M=16*3=48
	};
	
	/* cc: concentration */
	static int Linear(double AQIhigh, double AQIlow, double cchigh, double cclow, double cc) {
		double AQI = (cc-cclow)/(cchigh-cclow)*(AQIhigh-AQIlow)+AQIlow;
		return (int)Math.round(AQI);
	}
	
	public static int AQICategory(int AQI) {
		int AQICategory = 0;
		if (AQI<=50)
		{
			AQICategory=R.string.aqi_good;
		}
		else if (AQI>50 && AQI<=100)
		{
			AQICategory=R.string.aqi_moderate;
		}
		else if (AQI>100 && AQI<=150)
		{
			AQICategory=R.string.aqi_sensitive;
		}
		else if (AQI>150 && AQI<=200)
		{
			AQICategory=R.string.aqi_unhealthy;
		}
		else if (AQI>200 && AQI<=300)
		{
			AQICategory=R.string.aqi_veryunhealthy;
		}
		else if (AQI>300 && AQI<=400)
		{
			AQICategory=R.string.aqi_hazardous;
		}
		else if (AQI>400 && AQI<=500)
		{
			AQICategory=R.string.aqi_hazardous;
		}
		else
		{
			AQICategory=R.string.aqi_outrange;
		}
		return AQICategory;
	}
	
	public static int AQIAdvisory(int AQI) {
		int advisory = 0;
		if (AQI<=50)
		{
			advisory=R.string.aqi_good_advisory;
		}
		else if (AQI>50 && AQI<=100)
		{
			advisory=R.string.aqi_moderate_advisory;
		}
		else if (AQI>100 && AQI<=150)
		{
			advisory=R.string.aqi_sensitive_advisory;
		}
		else if (AQI>150 && AQI<=200)
		{
			advisory=R.string.aqi_unhealthy_advisory;
		}
		else if (AQI>200 && AQI<=300)
		{
			advisory=R.string.aqi_veryunhealthy_advisory;
		}
		else
		{
			advisory=R.string.aqi_hazardous_advisory;
		}
		
		return advisory;
	}
	
	public static int AQILevel(int AQI) {
		int AQILevel = 0;
		if (AQI<=50)
		{
			AQILevel=1;
		}
		else if (AQI>50 && AQI<=100)
		{
			AQILevel=2;
		}
		else if (AQI>100 && AQI<=150)
		{
			AQILevel=3;
		}
		else if (AQI>150 && AQI<=200)
		{
			AQILevel=4;
		}
		else if (AQI>200 && AQI<=300)
		{
			AQILevel=5;
		}
		else
		{
			AQILevel=6;
		}
		return AQILevel;
	}

	@ColorRes
	public static int AQIColor(int AQI) {
		int color = android.R.color.black;
		if (AQI<=50)
		{
			color=R.color.aqicolor_good;
		}
		else if (AQI>50 && AQI<=100)
		{
			color=R.color.aqicolor_moderate;
		}
		else if (AQI>100 && AQI<=150)
		{
			color=R.color.aqicolor_sensitive;
		}
		else if (AQI>150 && AQI<=200)
		{
			color=R.color.aqicolor_unhealthy;
		}
		else if (AQI>200 && AQI<=300)
		{
			color=R.color.aqicolor_veryunhealthy;
		}
		else
		{
			color=R.color.aqicolor_hazardous;
		}
		return color;
	}
	
	public static int getAQI(String name, float value, String standard) {
		int index = 0;
		for( index = 0; index < GlobalData.gItemNameList.length; index++) {
			if( name.compareTo( GlobalData.gItemNameList[index] ) == 0 ) break;
		}
		if( index >= GlobalData.gItemNameList.length ) {
			return 0;
		}
		
		int aqi = 0, bp;
		double [] breakpoints = Pollutant_Breakpoints_US[index];
		if( standard.compareTo(CN_STANDARD)==0)
		    breakpoints = Pollutant_Breakpoints_CN[index];
		for( bp = 1; bp < breakpoints.length; bp ++ ) {
			if( value >= breakpoints[bp-1] && value <= breakpoints[bp] ) {
				aqi = Linear(AQI_Breakpoints[bp],AQI_Breakpoints[bp-1],breakpoints[bp],breakpoints[bp-1],value);
				break;
			}
		}
		if( bp >= breakpoints.length )
		    aqi = OUTRANGE;
		
		return aqi;
	}
}
